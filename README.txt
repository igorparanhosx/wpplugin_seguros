=== Plugin Name ===
Contributors: (plugins2xt)
Donate link: https://segurospromo.com.br
Tags: segurospromo, seguros, promo, afiliado, viagem
Requires at least: 3.4.2
Tested up to: 4.9.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Stable tag: 1.5.0

O Plugin Seguros Promo permite aos blogs enriquecer o conteúdo dos seus textos com a inclusão de preços de seguros viagem que são atualizados constantemente pelas nossas APIs.

Além de deixar o conteúdo mais rico e melhorar a indexação das páginas com conteúdos atualizados, nossos afiliados também são REMUNERADOS pelas vendas provenientes dos seus blogs.

Não perca tempo, instale nosso plugin, seja um PARCEIRO PROMO e começe a lucrar como muitos outros já fazem.


== Description ==

O Plugin Seguros Promo permite aos blogs enriquecer o conteúdo dos seus textos com a inclusão de preços de seguros viagem que são atualizados constantemente pelas nossas APIs.

Além de deixar o conteúdo mais rico e melhorar a indexação das páginas com conteúdos atualizados, nossos afiliados também são REMUNERADOS pelas vendas provenientes dos seus blogs.

Não perca tempo, instale nosso plugin, seja um PARCEIRO PROMO e começe a lucrar como muitos outros já fazem.


== Installation ==

* É importante que você tenha o cadastro em nosso programa de afiliados para ter acesso ao seu Uid.
* Faça o upload do arquivo .Zip de dentro do próprio WordPress, configure o seu Uid de afiliado e comece a usar.
* No seu editor de post/página aparecerá um link para você adicionar o conteúdo de seguros viagem em seu texto.

== Changelog ==

**1.0**

Release date of - February 28, 2018