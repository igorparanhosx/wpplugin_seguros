var month_name = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
var follow = (seguros_promo.option_follow == 'disabled') ? ' rel="nofollow" ' : '';

if (checkCookie('spromo_last_origin')) {
    re_render_broker();
}
document.querySelectorAll('.spromo-origin-filter').forEach(
    function (elm) {
        elm.addEventListener('change', function () {

            var sc_url = elm.parentNode.parentNode.parentNode.getAttribute('data-shortcode'),
                utm_params = elm.parentNode.parentNode.parentNode.getAttribute('data-utm-params');
            sc_url = sc_url.replace('##', '@' + elm.value);
            render_body(sc_url, elm, utm_params);
            setCookie('spromo_last_origin', elm.value);

        })
    }
);

function re_render_broker() {
    document.querySelectorAll('.spromo-origin-filter').forEach(
        function (elm) {
            var sc_url = elm.parentNode.parentNode.parentNode.getAttribute('data-shortcode');
            var utm_params = elm.parentNode.parentNode.parentNode.getAttribute('data-utm-params');

            if (sc_url != '' && sc_url != null && typeof (sc_url) != 'undefined') {
                sc_url = sc_url.replace('##', '@' + getCookie('spromo_last_origin'));

                render_body(sc_url, elm, utm_params);
                changeSelection(elm, getCookie('spromo_last_origin'));
            }
        }
    );
}

function render_body(url, elm, utm_params, max_age) {

    var html = "",
        dados,
        req_url = url;

    if (typeof (max_age) !== 'undefined' && typeof (max_age) !== 'null') {
        if (url.slice(-1) == '?')
            req_url += 'max_age=' + max_age;
        else
            req_url += '&max_age=' + max_age;
    } else
        max_age = 0;

    fetch(req_url).then(function (response) {
        response.json().then(function (data) {
            data.forEach(function (d) {
                var dep_date = new Date(d.departure_date);
                var arr_date = new Date(d.returning_date);
                html += `
                    <a href="${d.ecommerce_url + '&' + utm_params}" target="_blank" class="comp-newshortcode__item" ${follow}>
                        <img class="logo_cia" src="${seguros_promo.plugin_dir + 'assets/cia/' + d.lowest_company_code + '.png'}" alt="">
                        <strong class="dep_iata">${d.departure_ap}</strong>
                        <small class="dep_city">${d.departure_ap_city}</small> 
                        <span class="arrow">&#8646;</span>
                        <small class="arr_city">${d.arrival_ap_city}</small>
                        <strong class="arr_iata">${d.arrival_ap}</strong>
                        <span class="dep_date">${dep_date.getUTCDate().toLocaleString('pt-BR', { minimumIntegerDigits: 2, useGrouping: false }) + ' ' + month_name[dep_date.getUTCMonth()]}</span>
                        <span class="separator">&bull;</span>
                        <span class="arr_date">${arr_date.getUTCDate().toLocaleString('pt-BR', { minimumIntegerDigits: 2, useGrouping: false }) + ' ' + month_name[arr_date.getUTCMonth()]}</span>
                        <span class="price">R$ ${Math.round(d.price_w_fees)}</span>
                    </a>
                `;

            });
            //document.querySelectorAll('.comp-newshortcode__ideal_para').innerHTML = data[0].arrival_ap_city.replace('-',' ');


            if (data.length >= 1 || max_age > 100)
                elm.parentNode.nextElementSibling.innerHTML = html;
            else
                render_body(url, elm, utm_params, max_age + 72);
        });
    })
}

document.querySelectorAll('.comp-newshortcode__item .selectbtn').forEach(
    function (elm) {
        elm.addEventListener('click', function () {
            var data = JSON.parse(elm.parentElement.getAttribute('data-json'));
            let ideal_para = (data['ideal_para'] != undefined ? data['ideal_para'] : 'brasil');
            let afilliate_id = data['afilliate_id'];
            let plan_code = data['code'];

            window.open();
        })
    }
);

// HELPERS
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie(cookie_name) {
    if (getCookie(cookie_name) != "" && getCookie(cookie_name) != null)
        return true;
    else
        return false;
}


// WIDGETS
document.querySelectorAll('.seguros-promo').forEach(
    function (elm) {
        if(elm.parentNode.offsetWidth <= 480){
            elm.querySelectorAll('.comp-newshortcode__item-card').forEach(
                function(item){
                    item.style.width = '98%';
                    item.style.marginBottom = '8px';
                }
            );
        }
    }
);