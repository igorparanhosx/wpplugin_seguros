(function() {

    tinymce.create('tinymce.plugins.SegurosPromoButtons', {
        init : function(ed, url) {

            url = url.slice(0, -3);

            ed.addButton('SegurosPromoButtons', {

                title : 'Seguros Promo',
                cmd: 'seguros_promo_conf_editor',
                image : url + '/assets/seguros_promo_icon.png'
            });

            ed.addCommand('seguros_promo_conf_editor', function() {

                seguros_promo_conf_editor('visual');

            });

        },
    });
    // Register plugin
    tinymce.PluginManager.add('SegurosPromoButtons', tinymce.plugins.SegurosPromoButtons);

})();
