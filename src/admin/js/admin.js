if (typeof (spromo_seguros_modal_html) == 'undefined' && spromo_seguros_modal_html != '') {

    var spromo_options_html;
    var spromo_options_widget_html;
    jQuery.ajax({
        url: 'https://www.segurospromo.com.br/emitir-seguros/v0/additional-info/great-for',
        method: 'GET',
        headers: {
            'Authorization': 'Basic c2VndXJvc3Byb21vOnNlZ3Vyb3Nwcm9tbw=='
        },
        datatype: 'json',
        success: function (data) {
            spromo_options_html = '<option value="" placeholder>Selecione a Região Ideal</option>';
            spromo_options_widget_html = '<option value="" placeholder>Selecione a Região Ideal</option>';
            for (let i = 0; i < data.length; i++) {
                spromo_options_html += '<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>';
                spromo_options_widget_html += '<option value="' + data[i]['id'] + '||' + data[i]['name'] + '">' + data[i]['name'] + '</option>';
            }
            $('.spromo-select-widget-greatfor').html(spromo_options_widget_html);
        }
    });

    var spromo_seguros_modal_html = `
            <div id="spromo_custom_conf_overlay">

                <div class="comp-plgconf">
                    <div class="comp-plgconf__title">
                        <strong>Afiliados Seguros Promo</strong>
                        <a href="https://wordpress.org/plugins/seguros-promo/" target="_blank" alt="ajuda" title="ajuda">AJUDA</a>
                    </div>
                    <div class="comp-plgconf__body">

                        <div class="comp-plgconf__body__cities">


                            <div class="comp-plgconf__body__cities__city">
                                <div class="line-label">
                                    <strong class="title">Ideal Para
                                    </strong>
                                    <picture>
                                        <div class="tooltip">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                                                <g fill="none" fill-rule="evenodd">
                                                    <path d="M0 0h24v24H0z" />
                                                    <path fill="#007e47" fill-rule="nonzero" d="M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z"
                                                    />
                                                </g>
                                                <span class="tooltiptext">Destino ou região ideal para o seguro.</span>
                                            </svg>
                                        </div>
                                    </picture>
                                </div>

                                <div class="line-input">
                                    <select class="input-class spromo-ideal_para-ideal-para" name="spromo-ideal_para-ideal-para" id="spromo-ideal_para-ideal-para"></select>
                                </div>
                            </div>


                            <div class="comp-plgconf__body__cities__city">
                                <div class="line-label">
                                    <strong class="title">Tema
                                    </strong>
                                    <picture>
                                        <div class="tooltip">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                                                <g fill="none" fill-rule="evenodd">
                                                    <path d="M0 0h24v24H0z" />
                                                    <path fill="#007e47" fill-rule="nonzero" d="M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z"
                                                    />
                                                </g>
                                                <span class="tooltiptext">Selecione o modelo do seu comparador de preços.</span>
                                            </svg>
                                        </div>
                                    </picture>
                                </div>

                                <div class="line-input">
                                    <select class="input-class" name="spromo-theme" id="spromo-theme">
                                    <option value="card-large" selected>Card Grande (3 ofertas)</option>
                                    <option value="card-medium">Card Médio (2 ofertas)</option>
                                    </select>

                                </div>
                            </div>

                        </div>

                    <div class="comp-plgconf__body__border__bt"></div>


                    <div class="comp-plgconf__body__cities">

                    <div class="comp-plgconf__body__cities__city" id="spromo-origin-field">
                        <div class="line-label">
                            <strong class="title">Tags</strong>
                            <picture>
                                <div class="tooltip">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M0 0h24v24H0z" />
                                            <path fill="#007e47" fill-rule="nonzero" d="M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z"/>
                                        </g>
                                        <span class="tooltiptext">Use a tag para rastreio do seu link na sua Dash.</span>
                                    </svg>
                                </div>
                            </picture>
                        </div>
                        <div class="line-input">
                            <input id="spromo-tags" type="text" placeholder="Ex.: post europa">
                        </div>
                    </div>
                    <div class="comp-plgconf__body__cities__city" id="spromo-origin-field">
                        <div class="line-label">
                            <strong class="title">Cupom de Desconto</strong>
                            <picture>
                                <div class="tooltip">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24">
                                        <g fill="none" fill-rule="evenodd">
                                            <path d="M0 0h24v24H0z" />
                                            <path fill="#007e47" fill-rule="nonzero" d="M11 17h2v-6h-2v6zm1-15C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zM11 9h2V7h-2v2z"/>
                                        </g>
                                        <span class="tooltiptext">Use o cupom de desconto para aplicar desconto automaticamente às pessoas que acessam o Seguros Promo através de seu blog.</span>
                                    </svg>
                                </div>
                            </picture>
                        </div>
                        <div class="line-input">
                            <input id="spromo-coupon" type="text" class="input-class" placeholder="Ex.: PROMO5">
                        </div>
                    </div>
                </div>
                    <div class="comp-plgconf__body__border__bt"></div>

                    <div class="comp-plgconf__footer">
                        <a href="" id="spromo-cancel" class="cancel">Cancelar</a>
                        <a href="" id="spromo_insert" class="insert">Inserir</a>
                    </div>
                </div>
            </div>
        `;
}


var spromo_tinymce_mode = '';

window.seguros_promo_conf_editor = (mode) => {
    spromo_tinymce_mode = mode || '';
    jQuery('body').append(spromo_seguros_modal_html);
    seguros_promo_conf_init();
}

function seguros_promo_conf_init() {
    // var broker_api_url = 'https://broker.api.emitir.com.br/fetch/';
    //MAYBE THE ERROR IS HERE
    jQuery(".spromo-ideal_para-ideal-para").html(spromo_options_html);

    const origin_cities = new Choices(jQuery('#spromo-ideal_para-ideal-para')[0], {
        itemSelectText: '',
        noResultsText: 'nenhum item encontrado',
        shouldSort: false,
        loadingText: 'Carregando...'
    });

    const theme = new Choices(jQuery('#spromo-theme')[0], {
        itemSelectText: '',
        noResultsText: 'nenhum item encontrado',
        shouldSort: false,
        loadingText: 'Carregando...'
    });

    const tags = new Choices(document.getElementById('spromo-tags'), {
        delimiter: ',',
        removeItemButton: true,
        duplicateItems: false,
        editItems: true,
        itemSelectText: "Aperte 'Enter' para selecionar...",
        addItemText: function (value) {
            return `Aperte 'Enter' para selecionar <b>"${value}"</b>`;
        },
        classNames: {
            button: 'choices__button',
        }
    });

    jQuery('#spromo_insert').click(function (event) {
        event.preventDefault();
        spromo_insert();
    });

    jQuery('#spromo-cancel').click(function (event) {
        event.preventDefault();
        spromo_exit_modal();
    });

}


function spromo_switch_visibility(elm1, elm2, priority) {
    if (priority === 1) {
        elm2.toggleClass('is-hidden', true);
        elm1.toggleClass('is-hidden', false);
    } else if (priority === 2) {
        elm2.toggleClass('is-hidden', false);
        elm1.toggleClass('is-hidden', true);
    }
}

function spromo_exit_modal() {
    jQuery('#spromo_custom_conf_overlay').remove();
}

function spromo_insert() {

    spromo_insert_shortcode();
    spromo_exit_modal();

}

function loadMonths() {
    var c_date = new Date();
    var now = c_date.getMonth();
    var month_choices = new Array();
    var months = [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
    ];

    months.forEach(function (val, i) {
        var index = now % 12;
        month_choices[i] = {
            value: (index + 1).toLocaleString('pt-BR', {
                minimumIntegerDigits: 2,
                useGrouping: false
            }),
            label: months[index] + ((now >= 12) ? ' ' + (c_date.getFullYear() + 1) : '')
        };
        now++;
    });
    return month_choices;
}

function spromo_format_shortcode(sc_params) {

    shortcode = '[seguros_promo_shortcode ' +
        ((sc_params.ideal_para != '') ? ' ideal_para="' + sc_params.ideal_para + '" ' : '') +
        ((sc_params.ideal_para_lbl != '') ? ' ideal_para_lbl="' + sc_params.ideal_para_lbl + '" ' : '') +
        ((sc_params.header_text != '') ? ' header_text="' + sc_params.header_text + '" ' : '') +
        ((sc_params['tags'] != '') ? ' tags="' + sc_params['tags'] + '" ' : '') +
        ((sc_params.theme != '') ? ' theme="' + sc_params.theme + '"' : '') +
        ((sc_params.coupon != '') ? ' coupon="' + sc_params.coupon + '"' : '') +
        '/]';

    return shortcode;
}

function spromo_insert_shortcode() {
    if (jQuery('#spromo-ideal_para-ideal-para').val() !== null && jQuery('#spromo-ideal_para-ideal-para').val() !== "") {
        var ideal_para = jQuery('#spromo-ideal_para-ideal-para').val();
        var ideal_para_lbl = jQuery('#spromo-ideal_para-ideal-para').text();
    } else {
        var ideal_para = "9";
        var ideal_para_lbl = "Brasil";
    }

    var sc_params = {
        ideal_para: ideal_para,
        ideal_para_lbl: ideal_para_lbl,
        header_text: jQuery('#spromo-title').val() || '',
        tags: jQuery('#spromo-tags').val() || '',
        theme: jQuery('#spromo-theme').val() || '',
        coupon: jQuery('#spromo-coupon').val() || '',
    };

    var formatted_shortcode = spromo_format_shortcode(sc_params);


    if (spromo_tinymce_mode === 'visual')
        top.tinymce.execCommand('mceInsertContent', 0, formatted_shortcode);
    else
        QTags.insertContent(formatted_shortcode);
}


function spromo_validate(required_inputs) {

    var flag = true;

    for (e in required_inputs) {

        elm = '#' + e;

        if (typeof (required_inputs[e].parent_selector) != 'undefined' && required_inputs[e].parent_selector != '')
            parent_elm = jQuery('#' + required_inputs[e].parent_selector);
        else
            parent_elm = jQuery(elm).parent();

        if (
            (
                jQuery(elm).val().match(required_inputs[e].pattern) == null &&
                (
                    required_inputs[e].pattern != '' &&
                    required_inputs[e].pattern != null
                )
            ) ||
            (
                (
                    required_inputs[e].pattern == '' ||
                    required_inputs[e].pattern == null
                ) &&
                (jQuery(elm).val() == '' || jQuery(elm).val() == null)
            )
        ) {
            if (required_inputs[e].msg != '' && required_inputs[e].msg !== undefined && !parent_elm.hasClass('is-invalid')) jQuery(elm).after('<div id="required_message_' + e + '" class="is-invalid-msg">' + required_inputs[e].msg + '</span >');
            parent_elm.toggleClass('is-invalid', true);

            flag = false;
        } else {
            parent_elm.toggleClass('is-invalid', false);
            jQuery('#required_message_' + e).remove();
        }
    }
    return flag;
}