import InspectorBlock from './block/inspector';
import GreatForPreview from './block/placeholder';

const { __ } = wp.i18n;
const { Fragment } = wp.element;
const { registerBlockType } = wp.blocks;
const {
    InspectorControls,
} = wp.editor;


const fmtShortcode = ({ name, ...params }) => {
    const paramsKeys = Object.keys(params)
    let fmtParams = ''
    paramsKeys.forEach(
        key => fmtParams += params[key] ? ` ${key}="${params[key]}"` : ''
    )

    return `[${name}${fmtParams} /]`
}

registerBlockType("seguros-promo/shortcode-block", {
    title: "Seguros Promo",
    icon: <img src={`${conf.iconPath}`} />,
    category: "common",
    keywords: [__("segurospromo")],

    attributes: {
        theme: {
            default: "card-large",
        },
        ideal_para: {
            default: "6",
        },
        ideal_para_lbl: {
            default: "América do Norte",
        },
        coupon: {
            default: "",
        },
        tags: {
            default: "",
        },
    },

    edit(props) {
        console.log(props.attributes)
        return [
            <InspectorControls>
                <InspectorBlock {...props} />
            </InspectorControls>,
            <Fragment>
                <GreatForPreview assetsPath={conf.assetsPath} {...props} />
            </Fragment>
        ]
    },

    save(props) {
        const {
            tags,
            theme,
            coupon,
            ideal_para,
            ideal_para_lbl,
        } = props.attributes;

        let origin = '';

        let destination = '';

        let shortcodeString = fmtShortcode({
          name: "seguros_promo_shortcode",
          tags,
          theme,
          coupon,
          ideal_para,
          ideal_para_lbl
        });
        console.log(shortcodeString)
        return shortcodeString
    }
});
