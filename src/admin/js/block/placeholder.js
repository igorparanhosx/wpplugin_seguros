import { slugify, getRedirectUrl, getFmtContext } from "./utils";

const { Component } = wp.element;


const CardItem = ({
	width,
	greatfor,
	assetsPath,
	redirectUrl,
	valuePerDay,
	coverageValues,
}) => (
	<div style={{ width: width }} className="comp-newshortcode__item-card" >
		<img className="logo_cia" src={`${assetsPath}/cia/${slugify(greatfor.provider_name)}.png`} alt="" />
		<small className="seg_name">   {greatfor.name}   </small>

		{
			greatfor.extra.special &&
				<small className="best-deal-svg">
					<svg xmlns="http://www.w3.org/2000/svg" width="38" height="35" viewBox="0 0 38 35">
						<g fill="none" fill-rule="evenodd">
							<path fill="#FFD012" d="M.75.543h36.5v21.214L23 32.831c-2.181 1.695-5.72 1.7-7.898.018L.75 21.757V.543z" />
							<path fill="#68629C" d="M25.813 7.127a.636.636 0 0 0-.45-.188l-1.82.002.009-.787a.64.64 0 0 0-.636-.649L15.108 5.5a.642.642 0 0 0-.636.643v.798l-1.835-.002a.636.636 0 0 0-.637.642v2.673c0 1.564 1.182 2.837 2.634 2.837.125 0 .248-.013.371-.032.666 1.253 1.77 2.121 3.219 2.33v3.042h-3.112c-.352 0-1.556.406-1.556.76v3.044c0 .354.863.765 1.214.765l8.9-.005c.352 0 .778-.406.778-.76v-3.043c0-.355-.426-.761-.778-.761h-3.89v-3.043c1.45-.208 2.525-1.079 3.19-2.333.131.021.263.036.396.036 1.452 0 2.634-1.273 2.634-2.837V7.58a.646.646 0 0 0-.187-.454zm-2.921 14.347h-7.78v-1.522h7.78v1.522zm-9.336-11.411V8.54h.778v2.283c0 .302.168.687.224.973-.714-.046-1.002-.908-1.002-1.734zm8.558.76c0 1.805-1.324 3.044-3.112 3.044s-3.112-1.239-3.112-3.043V7.78c.019-.061 0 .066 0 0 0-.067.019.06 0 0V7.02h6.224v3.804zm2.334 0c0 .837-.051.73-.778.761.055-.285 0-.46 0-.76V8.54h.778v2.283z" />
						</g>
					</svg>
				</small>
		}

		<small className="seg_division"></small>
		<small className="seg_assist_med">  Assistência médica </small>
		<small className="seg_assist_med_val">  {coverageValues[0]}  </small>
		<small className="seg_bag_ext">  Bagagem extraviada   </small>
		<small className="seg_bag_ext_val">  {coverageValues[1]}  </small>
		<a href={redirectUrl} target="_blank" className="selectbtn">
			R$ <b> {valuePerDay} </b>/dia*
			<span className="rightarrow">
				<svg xmlns="http://www.w3.org/2000/svg" width="12" height="10" viewBox="0 0 12 10">
					<path
						fill="#FFF"
						fill-rule="evenodd"
						d="M11.558 6.017L7.823 9.58a1.563 1.563 0 0 1-2.135 0 1.392 1.392 0 0 1 0-2.036L6.87 6.418h-5.37C.67 6.418 0 5.778 0 4.988c0-.789.67-1.43 1.499-1.43h5.346L5.688 2.457a1.39 1.39 0 0 1 0-2.035 1.563 1.563 0 0 1 2.135 0l3.735 3.562a1.392 1.392 0 0 1 0 2.034z" />
				</svg>
			</span>
		</a>
	</div>
)

class GreatForPreview extends Component {
	constructor(props) {
		super(props)

		this.state = {
			greatForList: []
		}
	}

	componentDidMount() {
		this.fetchGreatFor(this.props)
	}

	componentWillReceiveProps(nextProps) {
		this.fetchGreatFor(nextProps);
	}

	fetchGreatFor(props) {
		const url = 'http://localhost:8080/emitir-seguros/v1/quotations'
		const idealPara = props.attributes.ideal_para
		const body = {
			end_date: '2018-05-09',
			begin_date: '2018-05-02',
			benefits: ['51', '42'],
			greatfor: parseInt(idealPara),
		}

		const fetchOptions = {
			method: 'POST',
			body: JSON.stringify(body),
			headers: {
				Authorization: 'Basic c2VndXJvc3Byb21vOnNlZ3Vyb3Nwcm9tbw==',
			}
		}

		fetch(url, fetchOptions)
			.then(res => res.json())
			.then(json => {
				let greatForList = json

				if (json.length <= 2)
					greatForList = json
				else if (props.attributes.theme === 'card-large')
					greatForList = [json[0], json[1], json[2]]

				this.setState({
					greatForList
				})
			}
		)
	}

	render() {
		let className = "comp-newshortcode comp-newshortcode--mod01 comp-newshortcode--notitle"
		if (this.props.attributes.showLogo)
			className += "comp-newshortcode--nologo"

		const {
			greatForList
		} = this.state

		const {
			assetsPath,
			affiliateId,
		} = this.props

		const {
			tags,
			theme,
			coupon,
			ideal_para_lbl,
		} = this.props.attributes

		let width = ''
		if (greatForList.length === 1)
			width = '98%'
		else if (greatForList.length === 2)
			width = '48%'
		else if (theme === 'card-large' && greatForList.length >=3)
			width = '31%'

		// redirect
		const homeRedirectLink = `https://www.segurospromo.com.br/p/id-${affiliateId}/parceiro?tt=${tags}&cupom=${coupon}&utm_source=wp_plugin&utm_medium=afiliado`

		return (
			<div className="seguros-promo">
				<div className={className}>

					<div className="comp-newshortcode__header comp-newshortcode__header-card">
						<strong className="comp-newshortcode__header__subtitle">Seguro Viagem: <span className="comp-newshortcode__ideal_para"> {ideal_para_lbl} </span></strong>
						<small className="comp-newshortcode__header__logo">
							<a href={homeRedirectLink} target="_blank">
								<svg  xmlns="http://www.w3.org/2000/svg" width="375" height="64" viewBox="0 0 375 64">
									<defs>
										<path id="a" d="M.854 50h21.795V.133H.854z"/>
									</defs>
									<g fill="none" fill-rule="evenodd">
										<path fill="#FFF" d="M21.442 3.527c0 .987-.422 2.257-1.41 3.315-.988-.776-3.669-1.763-6.912-1.763-4.162 0-6.842 1.833-6.842 4.866 0 2.61 1.129 4.444 3.245 6.983l7.617 8.676c3.668 4.373 4.726 7.547 4.726 11.92 0 7.124-3.034 13.26-12.627 13.26C2.821 50.785 0 48.529 0 46.553c0-1.128.493-2.821 1.482-4.09 1.48 1.34 3.878 2.468 7.264 2.468 5.008 0 6.912-2.892 6.912-7.194 0-3.245-.986-5.572-4.09-9.17l-7.124-8.252C1.269 16.364.07 13.472.07 10.157.07 4.232 4.303 0 12.203 0c6.7 0 9.24 1.552 9.24 3.527M31.67 31.105c2.68.071 7.052-.07 9.521-.776.282-1.269.353-2.892.353-4.232-.07-5.078-.987-6.983-4.02-6.983-2.892 0-5.431 2.47-5.855 11.991m15.448-5.643c0 3.104-.212 6.631-.776 9.17-4.867.917-11.357 1.128-14.742 1.128.282 7.477 2.257 9.452 6.56 9.452 2.82 0 5.642-.846 8.04-2.187.352 1.06.494 2.751.494 3.88 0 2.186-4.444 3.88-9.733 3.88-6.56 0-11.287-2.752-11.287-16.506 0-17.633 6.279-20.173 12.485-20.173 5.925 0 8.959 3.74 8.959 11.356M57.767 26.873c0 5.008.847 7.97 5.22 7.97 3.738 0 5.36-3.174 5.36-9.31 0-5.149-1.763-7.195-5.148-7.195-3.034 0-5.432 2.328-5.432 8.535M60.236 48.6c-2.045 1.55-3.81 4.02-3.81 6.348 0 2.185 1.059 3.737 5.926 3.737 5.008 0 7.194-2.962 7.194-5.43 0-3.174-1.833-4.091-8.04-4.514-.424 0-.847-.072-1.27-.142m6.7-33.926H75.4c.282.493.564 1.41.564 2.75 0 1.48-.776 2.186-1.975 2.186-.705 0-1.622-.14-2.54-.352 1.623 1.622 2.68 4.16 2.68 7.617 0 7.477-3.243 12.344-11.425 12.344-1.2 0-2.257-.07-3.174-.282-.917.916-1.834 2.186-1.834 3.103 0 1.27 3.597 1.553 8.323 2.188 5.572.846 9.31 2.68 9.31 8.252 0 6.066-4.09 11.496-13.542 11.496s-11.286-4.02-11.286-7.688c0-3.456 2.328-6.419 5.36-8.464-2.68-.776-4.372-2.046-4.302-3.88.07-1.904 1.834-4.16 4.514-6.348-3.315-2.116-4.373-6.066-4.373-10.58 0-9.38 4.797-12.907 11.709-12.907.917 0 2.328.212 3.526.565M100.863 48.245c-2.68 1.481-6.701 2.54-10.51 2.54-8.393 0-10.58-3.81-10.58-11.709V16.011c0-1.199 0-1.199 5.925-1.199V39.57c0 4.584.917 6.207 4.726 6.207 1.27 0 3.245-.282 4.726-1.27V16.011c0-1.199.07-1.199 5.713-1.199v33.433zM107.352 17.28c2.751-1.903 6.137-3.173 9.593-3.173 3.597 0 4.938 1.058 4.938 2.963 0 .775-.14 2.186-.635 3.385a7.093 7.093 0 0 0-3.386-.846c-1.552 0-3.315.493-4.656 1.622l-.07 27.72c0 1.128-.07 1.128-5.784 1.128V17.28zM134.226 45.918c3.033 0 5.22-2.61 5.22-13.613 0-11.215-1.622-13.19-4.938-13.19-3.103 0-5.29 2.469-5.29 13.472 0 11.638 1.763 13.33 5.008 13.33m.494-31.81c7.406 0 10.933 3.809 10.933 18.127 0 13.12-4.02 18.55-11.568 18.55-7.265 0-11.074-2.468-11.074-18.338 0-12.908 4.302-18.34 11.71-18.34M169.846 18.057c0 .917-.494 2.045-1.27 2.892-2.116-1.27-4.232-1.834-6.912-1.834-3.316 0-5.572 1.199-5.572 3.386 0 1.269.423 2.398 2.821 4.373l6.56 5.29c3.033 2.962 4.161 5.29 4.161 8.817 0 6.277-4.444 9.804-11.638 9.804-5.008 0-8.605-1.128-8.605-3.597 0-.847.211-2.046.988-3.316 2.045 1.2 4.302 1.764 7.053 1.764 3.597 0 5.713-1.552 5.713-4.373 0-1.905-.705-3.598-2.82-5.502l-6.843-5.854c-3.174-2.61-4.02-5.15-4.02-7.477 0-4.937 3.597-8.323 11.497-8.323 7.264 0 8.887 2.539 8.887 3.95"/>
										<g transform="translate(174 13.975)">
											<mask id="b" fill="#fff">
												<use href="#a"/>
											</mask>
											<path fill="#FFF" d="M6.567 29.334c.987 1.622 2.398 2.328 3.809 2.328 2.186 0 5.925-1.552 5.925-16.224 0-8.394-1.834-10.298-5.431-10.298-1.694 0-3.104.494-4.303 1.34v22.854zM.854 2.954C3.745 1.118 7.343.131 11.151.131c6.984 0 11.498 3.103 11.498 14.671 0 18.973-5.713 22.007-10.51 22.007-2.61 0-4.514-.916-5.572-1.976v14.671c-.917.353-2.045.495-2.963.495-1.974 0-2.75-.565-2.75-2.468V2.953z" mask="url(#b)"/>
										</g>
										<path fill="#FFF" d="M201.798 17.28c2.75-1.903 6.136-3.173 9.593-3.173 3.597 0 4.936 1.058 4.936 2.963 0 .775-.14 2.186-.633 3.385a7.096 7.096 0 0 0-3.386-.846c-1.552 0-3.315.493-4.656 1.622l-.07 27.72c0 1.128-.071 1.128-5.784 1.128V17.28zM228.672 45.918c3.032 0 5.22-2.61 5.22-13.613 0-11.215-1.624-13.19-4.939-13.19-3.102 0-5.29 2.469-5.29 13.472 0 11.638 1.763 13.33 5.008 13.33m.495-31.81c7.405 0 10.933 3.809 10.933 18.127 0 13.12-4.022 18.55-11.569 18.55-7.264 0-11.073-2.468-11.073-18.338 0-12.908 4.302-18.34 11.709-18.34M258.93 22.642c-.07-2.54-.634-3.527-3.456-3.527-1.552 0-3.174.353-4.796 1.199V48.95c0 1.128-.21 1.128-5.713 1.128V16.435c3.103-1.411 6.771-2.328 10.65-2.328 3.033 0 5.008.776 6.348 2.187 2.4-1.34 5.784-2.187 8.676-2.187 7.053 0 7.9 3.598 7.9 9.946V48.95c0 1.128-.212 1.128-5.713 1.128V23.982c0-3.386-.635-4.796-3.457-4.796-1.622 0-3.173.352-4.726 1.34V48.95c0 1.128.071 1.128-5.713 1.128V22.642zM294.339 45.918c3.033 0 5.22-2.61 5.22-13.613 0-11.215-1.623-13.19-4.939-13.19-3.103 0-5.29 2.469-5.29 13.472 0 11.638 1.764 13.33 5.009 13.33m.493-31.81c7.406 0 10.933 3.809 10.933 18.127 0 13.12-4.02 18.55-11.567 18.55-7.265 0-11.075-2.468-11.075-18.338 0-12.908 4.303-18.34 11.71-18.34M374.222 25.373a31.784 31.784 0 0 0-1.428-4.873l-13.724 9.415-45.809-9.505a31.596 31.596 0 0 0-1.664 5.988l47.473 10.158 15.152-11.182z"/>
										<path fill="#FFF" d="M370.812 16.281C365.1 6.575 354.115-.322 342.064.036c-11.305.336-21.061 6.518-26.456 15.561l43.462 8.634 11.742-7.95zM374.841 29.994l-15.62 11.628.02.007-.022-.005-.009.007-.136-.033-.004-.005-47.944-10.043a32.153 32.153 0 0 0 .64 6.754l.156-.285 47.148 9.334 15.344-10.797c.34-1.93.452-4.535.427-6.562"/>
										<path fill="#FFF" d="M313.32 43.56c4.778 12.18 16.81 20.636 30.637 20.225 12.25-.363 24.347-7.752 29.26-21.63L359.07 52.416l-45.75-8.857z"/>
									</g>
								</svg>
							</a>
						</small>
					</div>
					<div className="comp-shortcode__body">
						<div className="com-shortcode__companies">
							{
								greatForList.map(
									greatfor => {
										const { coverageValues, valuePerDay } = getFmtContext(greatfor)
										const redirectUrl = getRedirectUrl(affiliateId, tags, coupon, greatfor.melhor_para, greatfor.code);

										return <CardItem width={width} assetsPath={assetsPath} coverageValues={coverageValues} redirectUrl={redirectUrl} valuePerDay={valuePerDay} greatfor={greatfor}/>
									}
								)
							}
						</div>
					</div>

					<div className="comp-newshortcode__footer-card">
						<small className="footer_ps">*Valor referente a 7 dias de viagem. Selecione o plano desejado!</small>
					</div>
				</div>
			</div>
		)
	}
}

export default GreatForPreview;
