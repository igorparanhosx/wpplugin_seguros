import makeAnimated from "react-select/lib/animated";
import CreatableSelect from "react-select/lib/Creatable";

const { __ } = wp.i18n;
const { Component } = wp.element;
const { PanelBody, PanelRow, SelectControl, TextControl, BaseControl } = wp.components;


const setGtLabel = (value, instance) => {
	const values = value.split('-')
	instance.props.setAttributes({
		ideal_para_lbl: values[0]
	})
	return values[1]
}

class TagsInput extends Component {
	constructor(props) {
		super(props)

		let value = []
		let valuesArray = []
		if (props.value) {
			valuesArray = props.value.split(',')
			value = valuesArray.map(value => ({ label: value, value }))
		}

		this.state = {
			value,
			valuesArray,
			inputValue: '',
		};
		this.handleChange = this.handleChange.bind(this)
		this.handleKeyDown = this.handleKeyDown.bind(this)
		this.handleInputChange = this.handleInputChange.bind(this)
		this.customStyle = {
			container: (provided) => ({
				...provided,
				minWidth: '100%'
			}),
			control: (provided) => ({
				...provided,
				minWidth: '100%',
				width: '100%',
				borderColor: '#a8a8a8'
			}),
			multiValueLabel: (provided) => ({
				...provided,
				background: '#186aaf',
				color: '#fff',
				borderTopRightRadius: 0,
				borderBottomRightRadius: 0,
			}),
			multiValueRemove: (provided) => ({
				...provided,
				background: '#186aaf',
				color: '#fff',
				borderTopLeftRadius: 0,
				borderBottomLeftRadius: 0,
			})
		}
	}
	handleChange(value, actionMeta) {
		this.setState({
			value,
			valuesArray: value.map(
				item => (item.label)
			)
		});
	};

	handleInputChange(inputValue) {
		this.setState({ inputValue });
	};

	handleKeyDown(event) {
		const { valuesArray, inputValue, value } = this.state;
		if (!inputValue) return;
		if (valuesArray.includes(inputValue)) return;
		switch (event.key) {
			case "Enter":
			case "Tab":
				this.setState({
					inputValue: "",
					value: [
						...value, {
							label: inputValue,
							value: inputValue,
						}
					],
					valuesArray: valuesArray.concat([inputValue])
				});
				this.props.onChange(valuesArray.concat([inputValue]).join(','));
				event.preventDefault();
		}
	};
	render() {
		const { inputValue, value } = this.state;
		return (
			<CreatableSelect
				isMulti
				isClearable
				value={value}
				menuIsOpen={false}
				components={makeAnimated({ DropdownIndicator: null })}
				inputValue={inputValue}
				styles={this.customStyle}
				onChange={this.handleChange}
				onKeyDown={this.handleKeyDown}
				onInputChange={this.handleInputChange}
				placeholder="Após digitar cada tag pressione ENTER..."
			/>
		);
	}
}

class InspectorComp extends Component {
	constructor(props) {
		super(props)

		const {
			tags,
			coupon,
			theme,
			ideal_para,
		} = props.attributes;

		const themeList = [
			{
				label: 'Card Grande (3 ofertas)',
				value: 'card-large'
			},
			{
				label: 'Card Médio (2 ofertas)',
				value: 'card-medium'
			}
		]

		this.state = {
			tags,
			coupon,
			theme,
			themeList,
			ideal_para,
			greatForList: [],
		}

		this.fetchGreatFor()
		this.changeAttr = this.changeAttr.bind(this)
	}

	fetchGreatFor() {
		const url = `https://www.segurospromo.com.br/emitir-seguros/v0/additional-info/great-for`;
		const fetchOptions = {
			method: 'GET',
			headers: {
				Authorization: 'Basic c2VndXJvc3Byb21vOnNlZ3Vyb3Nwcm9tbw=='
			}
		}
		fetch(url, fetchOptions)
			.then(res => res.json())
			.then(json => this.setState({
				greatForList: json.map(
					item => ({ label: item.name, value: `${item.name}-${item.id}` })
				)
			}))
	}

	changeAttr(name, middleware) {
		return value => {
			if (middleware) value = middleware(value, this);
			this.setState({ [name]: value })
			this.props.setAttributes({ [name]: value })
		}
	}

	render() {
		const fmtIdealPara = `${this.props.attributes.ideal_para_lbl}-${this.state.ideal_para}`;
		console.log(fmtIdealPara)
		return [
			<PanelBody title="Datas" initialOpen={true}>
				<PanelRow className="ppromo-block-inspector-section">
					<SelectControl
						label="IdealPara"
						value={fmtIdealPara}
						options={this.state.greatForList}
						onChange={this.changeAttr('ideal_para', setGtLabel)}
						className="ppromo-block-inspector-control"
						help="Escolha o destino ou finalidade para exibição"
					/>
				</PanelRow>
				<PanelRow className="ppromo-block-inspector-section">
					<TextControl
						label="Cupom de Desconto"
						value={this.state.coupon}
						onChange={this.changeAttr('coupon')}
						className="ppromo-block-inspector-control"
						help="Insira o código do cupom para ser aplicado aos links"
					/>
				</PanelRow>
			</PanelBody>,
			<PanelBody title="Geral" initialOpen={true}>
				<PanelRow className="ppromo-block-inspector-section">
					<SelectControl
						label="Tema"
						value={this.state.theme}
						options={this.state.themeList}
						onChange={this.changeAttr('theme')}
						className="ppromo-block-inspector-control"
						help="Escolha o tamanho e modelos de visualização do card"
					/>
				</PanelRow>
				<PanelRow className="ppromo-block-inspector-section">
					<BaseControl className="ppromo-block-inspector-control" label="Tags">
						<TagsInput
							value={this.state.tags}
							onChange={this.changeAttr('tags')} />
					</BaseControl>
				</PanelRow>
			</PanelBody>
		]
	}
}

export default InspectorComp;