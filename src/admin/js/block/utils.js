export const slugify = str => {
	str = str.replace(/^\s+|\s+$/g, ''); // trim
	str = str.toLowerCase();

	// remove accents, swap ñ for n, etc
	let from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
	let to = "aaaaeeeeiiiioooouuuunc------";
	for (let i = 0, l = from.length; i < l; i++) {
		str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}

	str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		.replace(/\s+/g, '-') // collapse whitespace and replace by -
		.replace(/-+/g, '-'); // collapse dashes

	return str;
}

export const getRedirectUrl = (affiliateId, tags, coupon, greatforId, code) => {
	const baseUrl = "https://www.segurospromo.com.br/p";
	const queryParams = `tt=${tags}&cupom=${coupon}&greatfor=${greatforId}&product_id=${code}&utm_source=wp_plugin&utm_medium=afiliado`;

	return `${baseUrl}/id-${affiliateId}/parceiro-ideal-para?${queryParams}`;
};

export const getFmtContext = greatfor => {
	if (!greatfor) return;

	let coverageValues = [];
	console.log(greatfor)
	if (greatfor.benefits[0] === "51")
		coverageValues = [
		greatfor.benefits[0].coverage,
		greatfor.benefits[1].coverage
		];
	else
		coverageValues = [
		greatfor.benefits[0].coverage,
		greatfor.benefits[1].coverage
		];

	coverageValues = coverageValues.map(item =>
		item
		.replace(",00", "")
		.replace("SUPLEMENTAR", "")
		.replace("COMPLEMENTAR", "")
	);

	const valuePerDay = Math.round(greatfor.adult.price / 7);

	return {
		valuePerDay,
		coverageValues
	};
};