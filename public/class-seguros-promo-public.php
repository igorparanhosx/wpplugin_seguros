<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://segurospromo.com.br
 * @since      1.0.0
 *
 * @package    Seguros_Promo
 * @subpackage Seguros_Promo/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Seguros_Promo
 * @subpackage Seguros_Promo/public
 * @author     2XT <plugins@2xt.com.br>
 */
class Seguros_Promo_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**     *
     * @since    1.0.0
     * @access   private
     * @var      string    $options
     */
    private $options;

    /**     *
     * @since    1.0.0
     * @access   private
     * @var      string    $option_name
     */
    private $option_name = 'seguros_promo';

    /**     *
     * @since    1.0.0
     * @access   private
     * @var      string    $api_url
     */

    private $api_url = 'https://www.segurospromo.com.br/emitir-seguros/v0';

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->options = get_option('seguros_promo');

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/public.css', array(), $this->version, 'all');

    }

    public function seguros_promo_public_add_shortcodes()
    {
        add_shortcode('seguros_promo_shortcode', array($this, $this->option_name . '_display_shortcode'));

    }

    public function seguros_promo_display_shortcode($attrs = array())
    {
        global $post;

        // if (has_shortcode($post->post_content, 'seguros_promo_shortcode')) {

        $seguros_promo_options = $this->options;
        $seguros_promo_public_path = plugin_dir_url(__FILE__);
        $seguros_promo_header_text = isset($attrs['header_text']) && $attrs['header_text'] != '' ? $attrs['header_text'] : '';
        $seguros_promo_theme = isset($attrs['theme']) && $attrs['theme'] != '' ? $attrs['theme'] : 'default';

        $requisition_url = $this->api_url . '/quotations';
        // $requisition_url = 'http://192.168.88.209:8080/emitir-seguros/v0/quotations';

        $requisition_headers = array(
            'Authorization' => 'Basic c2VndXJvc3Byb21vOnNlZ3Vyb3Nwcm9tbw==',
            'Content-Type' => 'application/json',
        );

        $requisition_body = array(
            "begin_date" => "2018-05-02",
            "end_date" => "2018-05-09",
            "greatfor" => (isset($attrs['ideal_para']) ? intval($attrs['ideal_para']) : 9),
            "benefits" => array("51", "42"),
        );

        $requisition = array(
            'body' => json_encode($requisition_body),
            'headers' => $requisition_headers,
        );

        $post_req = json_decode(wp_remote_retrieve_body(wp_remote_post($requisition_url, $requisition)));
        if (is_array($post_req)) {
            for ($i = 0; $i < count($post_req); $i++) {
                $post_req[$i]->saindo_de = 'brasil';
                $post_req[$i]->ideal_para = (isset($requisition_body['greatfor']) ? $requisition_body['greatfor'] : '9');
                $post_req[$i]->ideal_para_lbl = (isset($attrs['ideal_para_lbl']) ? $attrs['ideal_para_lbl'] : 'Brasil');
                $post_req[$i]->afilliate_id = (isset($this->options['affiliate_id']) ? $this->options['affiliate_id'] : '');
                $post_req[$i]->coupon = (isset($attrs['coupon']) ? $attrs['coupon'] : '');
                $post_req[$i]->tags = (isset($attrs['tags']) ? $attrs['tags'] : '');
            }

            $data = $post_req;

            ob_start();
            if(isset($attrs['theme']) && $attrs['theme'] == 'widget')
                include 'partials/seguros-promo-public-display-widget.php';
            else
                include 'partials/seguros-promo-public-display-card.php';

            $content = ob_get_clean();

            return $content;
        }

    }

    private function transform_month_pt_br($month_number)
    {
        if ($month_number >= 1 && $month_number <= 12) {
            $month_name = array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');
            return $month_name[$month_number - 1];
        } else {
            return '';
        }

    }

    private function get_origin_by_ip()
    {
        if (isset($_COOKIE['enderecoIp'])) {
            $url = "https://services.2xt.com.br/geoip/" . $_COOKIE['enderecoIp'];
        } else {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"]; // Pegando IP do usuário que acessou a página]

            setcookie('enderecoIp', $ip, (time() + (7 * 24 * 3600)), "/"); // Criando o Cookie
            $url = "https://services.2xt.com.br/geoip/" . $ip;
        }

        $manage = json_decode(wp_remote_retrieve_body(wp_remote_get($url)), true);
        $return = $manage['state'];

        return $return;
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        $follow_option = isset($this->options['follow']) ? $this->options['follow'] : 'enabled';

        wp_register_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/public.js');
        wp_register_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/serialize-0.2.min.js');

        wp_localize_script(
            $this->plugin_name,
            'seguros_promo',
            array('plugin_dir' => plugin_dir_url(__FILE__), 'option_follow' => $this->options['follow'])
        );

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/seguros-promo-public.js', array(), $this->version, 'all');
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/serialize-0.2.min.js', array(), $this->version, 'all');
    }
}


