const path = require("path");

const public_js = {
	entry: {
		public: "./src/public/js/public.js",
	},
	output: {
		filename: "[name].js",
		path: path.resolve(__dirname, "public/js")
	},
	mode: "development",
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader"
			}
		]
	}
};

const admin_js = {
	entry: {
		admin: "./src/admin/js/admin.js",
		tinymce: "./src/admin/js/tinymce.js",
		quicktags: "./src/admin/js/quicktags.js",
		blocks: "./src/admin/js/blocks.js",
	},
	output: {
		filename: "[name].js",
		path: path.resolve(__dirname, "admin/js")
	},
	mode: "development",
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: ["babel-loader"]
			}
		]
	}
};

const public_css = {
	entry: {
		public: "./src/public/scss/public.scss"
	},
	output: {
		path: path.resolve(__dirname, "public/css")
	},
	mode: "development",
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'public.css',
						}
					},
					{
						loader: 'extract-loader'
					},
					{
						loader: 'css-loader?-url'
					},
					{
						loader: 'sass-loader'
					}
				]
			}
		]
	},
};

const admin_css = {
	entry: {
		path: "./src/admin/scss/admin.scss"
	},
	output: {
		path: path.resolve(__dirname, "admin/css")
	},
	mode: "development",
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'admin.css',
						}
					},
					{
						loader: 'extract-loader'
					},
					{
						loader: 'css-loader?-url'
					},
					{
						loader: 'sass-loader'
					}
				]
			}
		]
	},
};

const config = [
	public_js,
	admin_js,
	public_css,
	admin_css,

];

module.exports = config;
